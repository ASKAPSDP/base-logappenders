// std include
#include <string>

#ifndef ASKAP_LOGAPPENDERS_H
#define ASKAP_LOGAPPENDERS_H

  /// The name of the package
#define ASKAP_PACKAGE_NAME "logappenders"

/// askap namespace
namespace askap {
  // @return version of the package
  std::string getAskapPackageVersion_logappenders();
}

/// The version of the package
#define ASKAP_PACKAGE_VERSION askap::getAskapPackageVersion_logappenders()

#endif
