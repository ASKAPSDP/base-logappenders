# @file
# build script for AutoBuild

from askapdev.rbuild.builders import Scons as Builder

# python based functest
from askapdev.rbuild.builders import Builder as Parent
Builder._functest = Parent._functest

b = Builder(".")
b.build()

