# Usage: ./run path_to_base-logappenders_repo
# e.g: ./run /askapbuffer/payne/mvuong/workspace/AXA-678/base-logappenders

if [ "$#" == 0 ] 
then
    # running in ci
    export INSTALLDIR=${CI_PROJECT_DIR}/_install
    export WORKDIR=${CI_PROJECT_DIR}/_build
    export REPO_ROOT_DIR=${CI_PROJECT_DIR}
else
    export INSTALLDIR=${1}/_install
    export WORKDIR=${1}/_build
    export REPO_ROOT_DIR=${1}
fi 

#echo "INSTALLDIR = ${INSTALLDIR}"
#echo "WORKDIR = ${WORKDIR}"

if [ ! -d ${INSTALLDIR} ]; then
    msg="Error: ${INSTALLDIR} does not exist"
    echo $msg
    exit 1
fi

# Running the testcase
cd ${REPO_ROOT_DIR}/functests
old_dir=`pwd`
# start the Ice services
./start_ice.sh ./iceregistry.cfg ./icegridadmin.cfg ./icestorm.cfg
sleep 5
# copy the configuration file to bin directory
cp tIceAppender.log_cfg ${INSTALLDIR}/bin/
cd ${INSTALLDIR}/bin
# run the log subscriber
./logsubscribe &
sleep 3
# get the process id of the logsubscriber process
subcriber_pid=`pidof logsubscribe`
# run the tIceAppender
LD_PRELOAD=$INSTALLDIR/lib/libaskap_logappenders.so ./tIceAppender
sleep 3
# remove the configuration file in the bin directory
rm ${INSTALLDIR}/bin/tIceAppender.log_cfg
# Stop the Ice Services
cd ${old_dir}
echo ""
echo "subcriber pid = ${subcriber_pid}"
# kill the logpublisher process
kill ${subcriber_pid}
# stop the Ice services
./stop_ice.sh ./icegridadmin.cfg


