/// @file IceAppenderTest
///
/// Simple unit test for severity level conversion
///
/// @copyright (c) 2007 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Max Voronkov <maxim.voronkov@csiro.au>
/// 
#ifndef ASKAP_ICE_APPENDER_TEST_H
#define ASKAP_ICE_APPENDER_TEST_H

#include <stdexcept>
#include <boost/shared_ptr.hpp>

#include "askap/iceappender/IceAppender.h"
#include "LoggingService.h"

#include <cppunit/extensions/HelperMacros.h>

namespace askap {

class IceAppenderTest : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(IceAppenderTest);
    CPPUNIT_TEST(testLevelConversion);
    CPPUNIT_TEST_SUITE_END();
public:
    void testLevelConversion()
    {
      // do nothing at this stage as we only test compilation
      CPPUNIT_ASSERT_EQUAL(askap::interfaces::logging::TRACE, IceAppender::convertLevel(log4cxx::Level::getTrace()));
      CPPUNIT_ASSERT_EQUAL(askap::interfaces::logging::DEBUG, IceAppender::convertLevel(log4cxx::Level::getDebug()));
      CPPUNIT_ASSERT_EQUAL(askap::interfaces::logging::INFO, IceAppender::convertLevel(log4cxx::Level::getInfo()));
      CPPUNIT_ASSERT_EQUAL(askap::interfaces::logging::WARN, IceAppender::convertLevel(log4cxx::Level::getWarn()));
      CPPUNIT_ASSERT_EQUAL(askap::interfaces::logging::ERROR, IceAppender::convertLevel(log4cxx::Level::getError()));
      CPPUNIT_ASSERT_EQUAL(askap::interfaces::logging::FATAL, IceAppender::convertLevel(log4cxx::Level::getFatal()));
      // unmapped level should translate to DEBUG, I am not sure about the best and/or portable way to test it but
      // it looks like there is a getOff method which returns some unmapped pointer (not sure about the intention as to
      // what this level is supposed to represent)
      CPPUNIT_ASSERT_EQUAL(askap::interfaces::logging::DEBUG, IceAppender::convertLevel(log4cxx::Level::getOff()));
    }
};
 
} // namespace askap

#endif // #ifndef ASKAP_ICE_APPENDER_TEST_H
